###*
Everslider - Responsive jQuery Carousel Plugin
http://plugins.gravitysign.com/everslider
Copyright (c) 2013 Roman Yurchuk
Version 1.6.1
###
(($) ->

  # Function to get vendor-specific CSS3 style property (e.g. WebkitTransition)
  # ---------------------------------------------------------------------------
  getVendorProperty = (name) ->
    property = false
    prefix = [
      "Webkit"
      "Moz"
      "O"
      "ms"
    ]
    test = document.createElement("div")

    # check unprefixed property
    if typeof test.style[name] is "string"
      property = name
    else
      name_u = name.charAt(0).toUpperCase() + name.substr(1)
      for p of prefix
        if typeof test.style[prefix[p] + name_u] is "string"
          property = prefix[p] + name_u
          break

    # prevent memory leaks in IE
    test = null

    # return property name or 'false'
    property

  # Function to get vendor specific CSS3 prefix (-webkit-,-moz-,-ms-,-o- ...)
  # ---------------------------------------------------------------------------
  getVendorPrefix = ->
    prefix =
      WebkitTransition: "-webkit-"
      MozTransition: "-moz-"
      msTransition: "-ms-"
      OTransition: "-o-"
      transition: ""


    # return "-webkit-" prefix for Safari and Chrome
    return prefix["WebkitTransition"]  if /(Safari|Chrome)/.test(navigator.userAgent)
    prefix[getVendorProperty("transition")]

  # Function to detect 3D support in browser
  # ---------------------------------------------------------------------------
  checkTransform3D = ->
    support = false
    test = document.createElement("div")
    transform = getVendorProperty("transform")

    # apply 3d property and get its value back
    test.style[transform] = "rotateY(45deg)"
    support = true  if test.style[transform] isnt ""

    # prevent memory leaks in IE
    test = null
    support

  # Function to get pixel offset for element (CSS3 "translate" or box model)
  # ---------------------------------------------------------------------------
  getPixelOffset = (element, cssok) ->
    transform = getVendorProperty("transform")
    position =
      left: 0
      top: 0

    if transform and cssok
      matrix = element.css(transform)
      if matrix.indexOf("matrix") is 0
        matrix = matrix.split("(")[1].split(")")[0].split(/,\s*/)
        position.left = parseInt(matrix[4], 10)
        position.top = parseInt(matrix[5], 10)
    else
      position = element.position()
    position

  # CSS3 variables
  # ---------------------------------------------------------------------------

  # Function to translate list position with "translate3d()"
  # ---------------------------------------------------------------------------
  translate = (element, pixels, animate) ->
    if typeof animate is "object"

      # setup transition for "transform" property
      property = cssprefix + "transform"
      setTransition element, property, animate.duration, animate.easing, animate.delay, animate.complete

      # run "animate.complete()" if there is no value change in CSS3 "transform"
      # property (in this case "transitionend" event won't fire)
      animate.complete.call element, property  if pixels is getPixelOffset(element, true).left

    # apply style
    if transform3d
      element.css transform, "translate3d(" + parseInt(pixels, 10) + "px, 0px, 0px)"
    else
      element.css transform, "translate(" + parseInt(pixels, 10) + "px, 0px)"
    return

  # Function to apply CSS3 transition to element. Additional "complete" function
  # can be provided to run as soon as transition ends
  # ---------------------------------------------------------------------------
  setTransition = (element, properties, duration, easing, delay, complete) ->

    # map jquery easing to cubic-bezier for CSS3 transition
    easing_map =
      linear: "linear"
      swing: "cubic-bezier(.02,.01,.47,1)"
      easeOutCubic: "cubic-bezier(.215,.61,.355,1)"
      easeInOutCubic: "cubic-bezier(.645,.045,.355,1)"
      easeInCirc: "cubic-bezier(.6,.04,.98,.335)"
      easeOutCirc: "cubic-bezier(.075,.82,.165,1)"
      easeInOutCirc: "cubic-bezier(.785,.135,.15,.86)"
      easeInExpo: "cubic-bezier(.95,.05,.795,.035)"
      easeOutExpo: "cubic-bezier(.19,1,.22,1)"
      easeInOutExpo: "cubic-bezier(1,0,0,1)"
      easeInQuad: "cubic-bezier(.55,.085,.68,.53)"
      easeOutQuad: "cubic-bezier(.25,.46,.45,.94)"
      easeInOutQuad: "cubic-bezier(.455,.03,.515,.955)"
      easeInQuart: "cubic-bezier(.895,.03,.685,.22)"
      easeOutQuart: "cubic-bezier(.165,.84,.44,1)"
      easeInOutQuart: "cubic-bezier(.77,0,.175,1)"
      easeInQuint: "cubic-bezier(.755,.05,.855,.06)"
      easeOutQuint: "cubic-bezier(.23,1,.32,1)"
      easeInOutQuint: "cubic-bezier(.86,0,.07,1)"
      easeInSine: "cubic-bezier(.47,0,.745,.715)"
      easeOutSine: "cubic-bezier(.39,.575,.565,1)"
      easeInOutSine: "cubic-bezier(.445,.05,.55,.95)"
      easeInBack: "cubic-bezier(.6,-.28,.735,.045)"
      easeOutBack: "cubic-bezier(.175, .885,.32,1.275)"
      easeInOutBack: "cubic-bezier(.68,-.55,.265,1.55)"


    # map css3 transition property to vendor specific event name
    event_map =
      transition: "transitionend"
      OTransition: "oTransitionEnd otransitionend"
      WebkitTransition: "webkitTransitionEnd"
      MozTransition: "transitionend"


    # argument defaults
    properties = properties.split(/\s+/)
    duration = (parseInt(duration, 10) / 1000 or 0) + "s"
    easing = easing_map[easing] or easing_map["swing"]

    # if delay has been ommited in favour of complete function
    if typeof delay is "function"
      complete = delay
      delay = 0
    delay = (parseInt(delay, 10) / 1000 or 0) + "s"
    complete = complete or $.noop

    # save old transition
    transition_tmp = element.css(transition)

    # register "transitionend" event handler
    element.bind event_map[transition], (e) ->
      event = e.originalEvent

      # make sure we deal with event target but not its descendants
      if event.target is this
        complete.call element, event.propertyName
        $(this).css(transition, transition_tmp).unbind e
      e.stopPropagation()
      return


    # set transition for element
    string = ""
    n = 0

    while n < properties.length
      string += properties[n] + " " + duration + " " + easing + " " + delay + ", "
      n++
    element.css transition, string.slice(0, -2)
    return

  # Constructor
  # ---------------------------------------------------------------------------
  Slider = (container, settings) ->
    @offset = 0
    @visible = 0
    @lock = false
    @timer = 0
    @api = {}
    @settings = settings
    @container = $(container)
    @list = @container.find("ul.es-slides")
    @total = @list.children("li").length
    @slide = @list.children("li:first-child")
    @cssok = getVendorProperty("transition") and @settings.useCSS
    self = this

    # return if .es-slides list doesn't exist or has no slides
    return null  if @total is 0

    # set move slides number
    if @settings.moveSlides is "auto"
      @settings.moveSlides = 9999 # will be forced to visible
    else
      @settings.moveSlides = parseInt(@settings.moveSlides, 10) or 1

    # set slide width (only >0)
    @settings.itemWidth = parseInt(@settings.itemWidth, 10) or 0
    @list.children().css "width", @settings.itemWidth  if @settings.itemWidth > 0

    # set slide height (only >0)
    @settings.itemHeight = parseInt(@settings.itemHeight, 10) or 0
    @list.children().css "height", @settings.itemHeight  if @settings.itemHeight > 0

    # set slide margin
    @list.children().css "margin-right", parseInt(@settings.itemMargin, 10) or 0  if @settings.itemMargin isnt false

    # if 'itemKeepRatio' is false set slide height to 'auto'
    @list.children().css height: "auto"  unless @settings.itemKeepRatio

    # save size of first slide for further calculations
    @slide_width = @slide.width()
    @slide_margin = parseInt(@slide.css("margin-right"), 10) or 0
    @slide_ratio = @slide.height() / @slide.width()

    # set viewport width
    if @settings.maxVisible > 0
      max_width = @settings.maxVisible * (@slide_width + @slide_margin) - @slide_margin
      @container.css "max-width", max_width
    else
      @container.css "max-width", @settings.maxWidth

    # carousel mode
    if @settings.mode is "carousel"
      cloned_before = @list.children().clone(true)
      cloned_after = @list.children().clone(true)

      # add cloned slides before and after original slides
      @list.prepend(document.createComment(" END CLONED ")).prepend(cloned_before).prepend document.createComment(" BEGIN CLONED ")
      @list.append(document.createComment(" BEGIN CLONED ")).append(cloned_after).append document.createComment(" END CLONED ")

      # change offset and total
      @offset = @total
      @total = @total * 3

      # set initial list offset
      p = @offset * (@slide_width + @slide_margin)
      if @cssok
        translate @list, -p
      else
        @list.css "left", -p

    # ticker
    @enableTicker()  if @settings.ticker and @settings.mode isnt "normal"

    # navigation
    if @settings.navigation
      @container.append [
        "<div class=\"es-navigation\">"
        "<a href=\"\" class=\"es-prev\">" + @settings.prevNav + "</a>"
        "<a href=\"\" class=\"es-next\">" + @settings.nextNav + "</a>"
        "</div>"
      ].join("\n")

      # update navigation
      @updateNavigation @offset

      # assing click handlers
      @container.find(".es-prev").click((e) ->
        self.slidePrevious()
        e.preventDefault()
        return
      ).end().find(".es-next").click((e) ->
        self.slideNext()
        e.preventDefault()
        return
      ).end()

    # pagination
    @container.append "<div class=\"es-pagination\"></div>"  if @settings.pagination and @settings.mode isnt "carousel"

    # touchSwipe navigation
    @enableTouchSwipe()  if @settings.touchSwipe

    # mousewheel navigation
    @enableMouseWheel()  if @settings.mouseWheel

    # keyboard navigation
    @enableKeyboard()  if @settings.keyboard

    # slide auto resize
    $(window).bind("resize", ->
      window.clearTimeout self.timer
      self.timer = window.setTimeout(->
        self.resizeSlides()
        return
      , self.settings.fitDelay)
      return
    ).trigger "resize"

    # create slider API
    $.extend @api,
      slideNext: ->
        self.slideNext()
        return

      slidePrevious: ->
        self.slidePrevious()
        return

      slideTo: (p) ->
        self.slideTo p
        return

      isSliding: ->
        self.isSliding()

      getVisibleSlides: ->
        self.getVisibleSlides()

      tickerPause: ->
        self.tickerPause()  if "tickerPause" of self
        return

      tickerPlay: ->
        self.tickerPlay()  if "tickerPlay" of self
        return


    # make API methods available through data and event
    @container.data "everslider", @api
    @container.bind "everslider", (e, method, param) ->
      self.api[method] param  if method of self.api
      false


    # when slides ready
    window.setTimeout (->

      # assign ready class to the container
      self.container.addClass "es-slides-ready"

      # add 'es-after-slide' class to visible slides
      self.getVisibleSlides().addClass "es-after-slide"

      # run slidesReady() callback
      self.settings.slidesReady.call self.container.get(0), self.api  if typeof self.settings.slidesReady is "function"
      return
    ), parseInt(@settings.fitDelay, 10) + parseInt(@settings.fitDuration, 10)
    return
  "use strict"
  transition = getVendorProperty("transition")
  transform = getVendorProperty("transform")
  cssprefix = getVendorPrefix()
  transform3d = checkTransform3D()

  # Method to move slider next (API method)
  # ---------------------------------------------------------------------------
  Slider::slideNext = ->
    @slideOffset @getOffset("next")  unless @lock
    return


  # Method to move slider previous (API method)
  # ---------------------------------------------------------------------------
  Slider::slidePrevious = ->
    @slideOffset @getOffset("prev")  unless @lock
    return


  # Method to move slider to specific zero-based position (API method)
  # ---------------------------------------------------------------------------
  Slider::slideTo = (p) ->

    # normalize position for "carousel" mode
    p = @total / 3 + Math.min(p, @total / 3 - @visible)  if @settings.mode is "carousel"
    position_offset = p - @offset
    direction = (if position_offset > 0 then "next" else "prev")

    # save original offset
    offset_tmp = @offset

    # shift carousel to requested position N times to get new offset
    n = 0

    while n < Math.abs(position_offset)
      @offset = @getOffset(direction)
      n++

    # save received offset
    offset = @offset
    @offset = offset_tmp

    # start main carousel transition
    @slideOffset offset
    return


  # Method to find out if carousel is sliding (API method)
  # ---------------------------------------------------------------------------
  Slider::isSliding = ->
    @lock


  # Method to get subset of slides that are visible at the moment (API method)
  # ---------------------------------------------------------------------------
  Slider::getVisibleSlides = ->
    @list.children().slice @offset, @offset + @visible


  # Method to get offset number based on slide direction
  # ---------------------------------------------------------------------------
  Slider::getOffset = (direction) ->

    # don't move more slides than visible
    slide_limit = Math.min(@settings.moveSlides, @visible)

    # get offset for "prev" direction
    if direction is "prev"
      if @settings.mode is "carousel" and @offset is 0
        p = @total / 3 * (@slide.width() + @slide_margin)
        if @cssok
          translate @list, -p  if @settings.effect isnt "fade"
        else
          @list.css "left", -p  if @settings.effect isnt "fade"
        return @total / 3 - slide_limit
      else if @settings.mode is "circular" and @offset is 0
        return @total - @visible
      else
        return @offset - ((if @offset > slide_limit then slide_limit else @offset))

    # get offset for "next" direction
    if direction is "next"
      left = @total - (@offset + @visible)
      if @settings.mode is "carousel" and left is 0
        p = (@offset - @total / 3) * (@slide.width() + @slide_margin)
        if @cssok
          translate @list, -p  if @settings.effect isnt "fade"
        else
          @list.css "left", -p  if @settings.effect isnt "fade"
        @offset - @total / 3 + slide_limit
      else if @settings.mode is "circular" and left is 0
        0
      else
        @offset + ((if left > slide_limit then slide_limit else left))


  # Method to change slider position by the amount of offset
  # ---------------------------------------------------------------------------
  Slider::slideOffset = (offset, force) ->

    # return if offset won't change (force param must be unset)
    return  if not force and offset is @offset
    self = this
    unlock = ->

      # unlock slides
      self.lock = false

      # save new offset
      self.offset = offset

      # callbacks and events that run after slide transition completes
      unless force

        # sync container height
        self.syncContainerHeight()

        # when transition has completed add 'es-after-slide' class to visible items
        self.list.children(".es-after-slide").removeClass "es-after-slide"
        self.getVisibleSlides().removeClass("es-before-slide").addClass("es-after-slide").trigger "es-after-slide"

        # run "afterSlide" callback
        self.settings.afterSlide.call self.container.get(0), self.getVisibleSlides()  if typeof self.settings.afterSlide is "function"
      return


    # set lock
    @lock = true

    # callbacks and events that run before slide transition begins
    unless force

      # add 'es-before-slide' class to the new visible items as well as trigger custom event
      @list.children().slice(offset, offset + @visible).not(".es-after-slide").addClass("es-before-slide").trigger "es-before-slide"

      # run "beforeSlide" callback
      @settings.beforeSlide.call @container.get(0), @getVisibleSlides()  if typeof @settings.beforeSlide is "function"

    # update pagination
    if @settings.pagination and @settings.mode isnt "carousel"
      slide_limit = Math.min(@settings.moveSlides, @visible)
      active_page = Math.ceil(offset / slide_limit)
      @container.find(".es-pagination a:eq(" + active_page + ")").addClass("es-active").siblings().removeClass "es-active"

    # update navigation
    @updateNavigation offset

    # get pixel offset by offset number
    pixel_offset = offset * (@slide.width() + @slide_margin)

    # the code below performs main carousel transition using either slide or fade effect;
    # code is split into two blocks: if browser is CSS3 compliant and if it's not, in which case it uses jQuery $.animate;
    # at the end of transition unlock() function will be executed to run callbacks
    if @cssok

      # 'fade' animation effect
      if @settings.effect is "fade"
        now_visible = @getVisibleSlides()
        next_visible = @list.children().slice(offset, offset + @visible)

        # when 'fadeDirection' plugin option is set to -1 we reverse fade direction
        if @settings.fadeDirection * offset > @offset * @settings.fadeDirection
          next_visible = Array::reverse.call(next_visible)
          now_visible = Array::reverse.call(now_visible)

        # hide currently visible slides using delayed fade-out animation
        $.each now_visible, (n) ->
          setTransition $(this), "opacity", self.settings.fadeDuration, self.settings.fadeEasing, self.settings.fadeDelay * n, ->
            return  if n < self.visible - 1 # wait for last slide
            # set opacity = 0 on slides that will be visible next moment;
            # translate list by amount of offset, then run fade-in animation for next slides
            next_visible.css "opacity", 0
            translate self.list, -pixel_offset,
              duration: 0
              easing: "linear"
              delay: 15
              complete: ->
                $.each next_visible, (n) ->
                  setTransition $(this), "opacity", self.settings.fadeDuration, self.settings.fadeEasing, self.settings.fadeDelay * n, ->
                    return  if n < self.visible - 1 # wait for last slide
                    now_visible.add(next_visible).css "opacity", ""
                    unlock()
                    return

                  $(this).css "opacity", 1
                  return

                return

            return

          $(this).css "opacity", 0
          return

      else

        # 'slide' animation effector
        translate @list, -pixel_offset,
          duration: @settings.slideDuration
          easing: @settings.slideEasing
          delay: @settings.slideDelay
          complete: unlock

    else

      # 'fade' animation effect (jQuery fallback)
      if @settings.effect is "fade"
        now_visible = @getVisibleSlides()
        next_visible = @list.children().slice(offset, offset + @visible)

        # when 'fadeDirection' plugin option is set to -1 we reverse fade direction
        if @settings.fadeDirection * offset > @offset * @settings.fadeDirection
          next_visible = Array::reverse.call(next_visible)
          now_visible = Array::reverse.call(now_visible)

        # hide currently visible slides using delayed fade-out animation
        $.each now_visible, (n) ->
          $(this).stop().delay(self.settings.fadeDelay * n).animate
            opacity: 0
          , self.settings.fadeDuration, self.settings.fadeEasing, ->
            return  if n < self.visible - 1 # wait for last slide
            next_visible.css "opacity", 0
            self.list.delay(10).queue ->
              $(this).css("left", -pixel_offset).dequeue()
              $.each next_visible, (n) ->
                $(this).stop().delay(self.settings.fadeDelay * n).animate
                  opacity: 1
                , self.settings.fadeDuration, self.settings.fadeEasing, ->
                  return  if n < self.visible - 1 # wait for last slide
                  now_visible.add(next_visible).css "opacity", ""
                  unlock()
                  return

                return

              return

            return

          return

      else

        # 'slide' animation effect (jQuery fallback)
        @list.stop().delay(@settings.slideDelay).animate
          left: -pixel_offset
        , @settings.slideDuration, @settings.slideEasing, unlock
    return

  # this.cssok

  # Method to resize slides when browser width is changed
  # ---------------------------------------------------------------------------
  Slider::resizeSlides = ->

    # lock resize
    @lock = true

    # get number of slides that would fit into container viewport
    @visible = @container.width() / (@slide_width + @slide_margin)
    if @visible % 1 is 0 or @visible % 1 < 0.5
      @visible = (if Math.floor(@visible) > 0 then Math.floor(@visible) else 1)
    else
      @visible = Math.ceil(@visible)

    # calculate slide size using new "visible" value
    width = (@container.width() + @slide_margin) / @visible - @slide_margin
    height = @slide_ratio * width
    size = width: Math.round(width)
    size.height = Math.round(height)  if @settings.itemKeepRatio

    # correct slide list offset
    if @offset > 0

      # make sure offset number is correct after new "visible" value
      @offset = @total - @visible  if @offset + @visible > @total
      pixel_offset = @offset * (width + @slide_margin)
      if @cssok
        translate @list, -pixel_offset
      else
        @list.css "left", -pixel_offset

    # prepare to animation
    self = this
    duration = @settings.fitDuration
    easing = @settings.fitEasing
    unlock = ->
      self.lock = false
      self.syncContainerHeight()
      return


    # run animation to change slides size
    @list.children().each ->
      if self.cssok

        # if width won't change run "unlock()" immediately
        if $(this).width() is Math.round(width)
          unlock()
        else
          setTransition $(this), "width height", duration, easing, unlock
          $(this).css size
      else
        $(this).stop().animate size, duration, easing, unlock
      return


    # update pagination
    @updatePagination()
    return


  # Method to sync container height with the highest visible slide
  # ---------------------------------------------------------------------------
  Slider::syncContainerHeight = ->
    if @settings.syncHeight and not @settings.itemKeepRatio

      # find highest visible slide
      max_height = 0
      $.each @getVisibleSlides(), ->
        max_height = $(this).height()  if $(this).height() > max_height
        return


      # animate container height
      duration = @settings.syncHeightDuration
      easing = @settings.syncHeightEasing
      if @cssok
        setTransition @container, "height", duration, easing
        @container.css "height", max_height
      else
        @container.stop().animate
          height: max_height
        , duration, easing
    return


  # Method to create/update pagination bullets
  # ---------------------------------------------------------------------------
  Slider::updatePagination = ->

    # do nothing if pagination is not enabled
    return  if not @settings.pagination or @settings.mode is "carousel"

    # calculate how many pages we need to display
    self = this
    slide_limit = Math.min(@settings.moveSlides, @visible)
    total_pages = Math.ceil(@total * 2 / (slide_limit + @visible))

    # re-add page bullets
    pagination = @container.find(".es-pagination").empty()
    i = 0

    while i < total_pages

      # check if not locked

      # offset cannot be more then "total - visible" slides!
      $("<a href=\"#\">" + i + "</a>").click(((index) ->
        (e) ->
          return  if self.lock
          offset = Math.min(index * slide_limit, self.total - self.visible)
          self.slideOffset offset
          e.preventDefault()
          return
      )(i)).appendTo pagination
      i++

    # figure out active bullet
    active_page = Math.ceil(@offset / slide_limit)
    pagination.find("a:eq(" + active_page + ")").addClass("es-active").siblings().removeClass "es-active"
    return


  # Method to update navigation controls
  # ---------------------------------------------------------------------------
  Slider::updateNavigation = (offset) ->

    # add classes to navigation to indicate reaching first/last slide
    if @settings.navigation and @settings.mode is "normal"
      navigation = @container.find(".es-navigation a")

      # add classes on first/last slide
      navigation.removeClass "es-first es-last"
      navigation.filter(".es-prev").addClass "es-first"  if offset is 0
      navigation.filter(".es-next").addClass "es-last"  if offset is @total - @visible
    return


  # Method to enable touchSwipe functionality
  # ---------------------------------------------------------------------------
  Slider::enableTouchSwipe = ->
    self = this
    swipe = false
    touch_x = 0
    touch_y = 0
    pixel_offset = 0
    swipeStart = (e) ->
      event = e
      event = e.originalEvent.changedTouches[0]  if e.type.indexOf("touch") is 0

      # start swipe if not locked
      unless self.lock
        swipe = true

        # save touch point position
        touch_x = event.pageX
        touch_y = event.pageY

        # save slides list position
        pixel_offset = getPixelOffset(self.list, self.cssok).left

        # bind move handler
        self.container.bind "mousemove touchmove", swipeMove

        # add grab class to container on swipe start
        self.container.addClass "es-swipe-grab"
      return

    swipeMove = (e) ->
      event = e
      event = e.originalEvent.changedTouches[0]  if e.type.indexOf("touch") is 0

      # get distance from touch point to current pointer position
      swipe_x = event.pageX - touch_x
      swipe_y = event.pageY - touch_y

      # check if threshold is exceeded
      if Math.abs(swipe_x) < self.settings.swipeThreshold

        # apply visual offset only for "slide" effect
        if self.settings.effect is "slide"
          if self.cssok
            translate self.list, pixel_offset + swipe_x
          else
            self.list.css "left", pixel_offset + swipe_x
      else

        # get swipe direction
        swipe_direction = (if (swipe_x > 0) then "prev" else "next")
        offset = self.getOffset(swipe_direction)

        # main carousel transition
        self.slideOffset offset

        # remove move handler
        self.container.unbind "mousemove touchmove", swipeMove

      # check if vertical swipe should scroll page
      e.preventDefault()  unless self.settings.swipePage
      return

    swipeEnd = ->
      if swipe

        # if carousel has moved slightly but threshold has not exceeded return it to previous position
        self.slideOffset self.offset, true  if not self.lock and pixel_offset isnt getPixelOffset(self.list, self.cssok).left

        # unbind move handler
        self.container.unbind "mousemove touchmove", swipeMove
        swipe = false

        # remove grab class from container on swipe end
        self.container.removeClass "es-swipe-grab"
      return


    # register event listeners
    @container.bind "mousedown touchstart", swipeStart
    $("body").bind "mouseup touchend touchcancel", swipeEnd

    # disable dragging
    @container.bind "dragstart", (e) ->
      e.preventDefault()
      return

    return


  # Method to enable mousewheel
  # ---------------------------------------------------------------------------
  Slider::enableMouseWheel = ->

    # check if mousewheel plugin is loaded
    return  if typeof $.fn.mousewheel isnt "function"

    # bind to mousewheel event
    self = this
    @container.bind "mousewheel", (e, delta) ->
      if delta > 0
        self.slidePrevious()
      else
        self.slideNext()
      e.preventDefault()
      return

    return


  # Method to enable keyboard navigation
  # ---------------------------------------------------------------------------
  Slider::enableKeyboard = ->
    self = this
    $(document).bind "keydown", (e) ->
      if e.which is 39
        self.slideNext()
      else self.slidePrevious()  if e.which is 37
      return

    return


  # Method to enable ticker (autoplay)
  # ---------------------------------------------------------------------------
  Slider::enableTicker = ->
    self = this
    first_run = true
    ticker_timer = undefined
    timeout = undefined
    delay = 0
    duration = 0
    ticker_timeout = parseInt(@settings.tickerTimeout, 10)

    # get delay and duration for animation effect
    if @settings.effect is "fade"
      delay = parseInt(@settings.fadeDelay, 10)
      duration = parseInt(@settings.fadeDuration, 10)
    else
      delay = parseInt(@settings.slideDelay, 10)
      duration = parseInt(@settings.slideDuration, 10)

    # functions to start ticker
    @tickerPlay = ->
      @container.find(".es-ticker a").hide().filter(".es-pause").show()

      # calculate timeout
      if first_run
        timeout = ticker_timeout
      else
        if self.settings.effect is "fade"
          timeout = ((self.visible - 1) * delay + self.visible * duration) + ticker_timeout
        else
          timeout = (delay + duration) + ticker_timeout

      # start timer
      window.clearInterval ticker_timer
      ticker_timer = window.setInterval(->
        self.slideNext()

        # after first run restart timer with new timeout
        if first_run
          first_run = false
          self.tickerPlay()
        return
      , timeout)
      return


    # function to pause ticker
    @tickerPause = ->
      @container.find(".es-ticker a").hide().filter(".es-play").show()

      # stop timer
      window.clearInterval ticker_timer
      first_run = true
      return


    # create play/pause controls
    @container.append "<div class=\"es-ticker\"></div>"
    $("<a href=\"#\" class=\"es-play\">" + @settings.tickerPlay + "</a>").click((e) ->
      self.tickerPlay()
      e.preventDefault()
      return
    ).appendTo @container.find(".es-ticker")
    $("<a href=\"#\" class=\"es-pause\">" + @settings.tickerPause + "</a>").click((e) ->
      self.tickerPause()
      e.preventDefault()
      return
    ).appendTo @container.find(".es-ticker")

    # pause ticker on hover (with delay)
    if @settings.tickerHover
      hover_timer = 0
      @container.hover (->
        window.clearTimeout hover_timer
        hover_timer = window.setTimeout(->
          self.tickerPause()
          return
        , self.settings.tickerHoverDelay)
        return
      ), ->
        window.clearTimeout hover_timer
        hover_timer = window.setTimeout(->
          self.tickerPlay()
          return
        , self.settings.tickerHoverDelay)
        return


    # stop ticker
    @tickerPause()

    # start ticker if needed
    @tickerPlay()  if @settings.tickerAutoStart
    return


  # ---------------------------------------------------------------------------
  # Register jQuery plugin
  # ---------------------------------------------------------------------------
  $.fn.everslider = (c) ->

    # default settings
    s = $.extend(
      mode: "normal" # carousel mode - 'normal', 'circular' or 'carousel'
      effect: "slide" # animation effect - 'slide' or 'fade'
      useCSS: true # set false to disable CSS3 transitions
      itemWidth: false # slide width, px (or false for css value)
      itemHeight: false # slide height, px (or false for css value)
      itemMargin: false # slide margin, px (or false for css value)
      itemKeepRatio: true # during resize always retain side ratio of a slide
      maxWidth: "100%" # max container width, px or %
      maxVisible: 0 # show only N slides (overrides maxWidth, 0 to ignore)
      moveSlides: 1 # number of slides to move or 'auto' to move all visible
      slideDelay: 0 # slide effect initial delay, ms
      slideDuration: 500 # slide effect duration, ms
      slideEasing: "swing" # slide effect easing
      fadeDelay: 200 # fade effect delay, ms
      fadeDuration: 500 # fade effect duration, ms
      fadeEasing: "swing" # fade effect easing
      fadeDirection: 1 # 1 is default, set -1 to start fade from opposite side
      fitDelay: 300 # slides fit (carousel resize) delay, ms
      fitDuration: 200 # slides fit (carousel resize) duration, ms
      fitEasing: "swing" # slides fit (carousel resize) easing
      syncHeight: false # sync carousel height with largest visible slide
      syncHeightDuration: 200 # carousel height sync duration, ms
      syncHeightEasing: "swing" # carousel height sync easing
      navigation: true # enable prev/next navigation
      nextNav: "<span>Next</span>" # next navigation control, text/html
      prevNav: "<span>Previous</span>" # previous navigation control, text/html
      pagination: true # enable pagination (only 'normal' or 'circular' mode)
      touchSwipe: true # enable touchSwipe
      swipeThreshold: 50 # pixels to exceed to start slide transition
      swipePage: false # allow touchswipe to scroll page also
      mouseWheel: false # enable mousewheel (requires jquery-mousewheel plugin)
      keyboard: false # enable keyboard left/right key navigation
      ticker: false # enable ticker ('circular' or 'carousel' mode)
      tickerTimeout: 2000 # ticker timeout, ms
      tickerAutoStart: true # start ticker when plugin loads
      tickerPlay: "<span>Play</span>" # ticker play control, text/html
      tickerPause: "<span>Pause</span>" # ticker pause control, text/html
      tickerHover: false # pause ticker on mousehover
      tickerHoverDelay: 300 # delay before ticker will pause on mousehover
      slidesReady: -> # slider ready callback

      beforeSlide: -> # before slide callback

      afterSlide: -> # after slide callback
    , c)
    @each ->

      # create slider instance
      new Slider(this, s)
      return


  return
) jQuery