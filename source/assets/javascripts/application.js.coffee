# *************************************
#
#   Application
#
# *************************************

# -------------------------------------
#   Components
# -------------------------------------
#= require components/_components.fastclick
#= require components/_components.everslider
# -------------------------------------
#   Inbox
# -------------------------------------
$ ->
  "use strict"
$("#main-promo").everslider
  mode: "normal"
  maxVisible: 1
  slideEasing: "easeInOutQuart"
  itemWidth: 1300
  navigation: false
  slideDuration: 500
